#!/bin/env lua
local api=require("luamark")



local function run (input)
   local f = io.popen(input) -- runs command
   local l = f:read("*a") -- read output of command
   f:close()
   return l
end


local function dmenu (input, ...)
   local args = ... or " "
   return run("echo \""..input.."\"|tr -d \" \"|rofi -dmenu ".. args)
end






local actions=table.concat({
"get",
"put",
"delete"
},"\n")


local function selection() return string.gsub(dmenu(api.list()),"\n","") end

local action=string.gsub(dmenu(actions),"\n","" )

if action=="get" then
   print(api.get(selection()))
elseif action=="getalias" then
   print(api.getAlias(selection()))
elseif action=="put" then
   local name  = dmenu(" ","-p name" )
   local path  = dmenu(" ","-p path" )
   api.put(name,path)
elseif action=="delete" then
   if string.gsub(dmenu("yes\nno"),"\n","") =="yes" then
	  print("deleting...")
	  api.remove(selection())
   end
end







