#!/bin/env lua
-- -*- tab-width: 4; indent-tabs-mode: t; lua-indent-level: 4; lua-indent-string-contents: t; -*-


local api=require("luamark")
--local markfile = "/home/erik/.config/luamark/bookmarks.json"
api.load()

_G.switch = function(param, case_table)
	local case = case_table[param]
	if case then
		return case()
	end
	local def = case_table['default']
	return def and def() or nil
end



local function main(arg)
	switch(arg[1],
		{
			["get"]=function()
				print(api.get(arg[2]))
			end,
			["put"]=function()
				api.put(arg[2],arg[3],arg[4])
			end,
			["set"]=function()
				api.put(arg[2],arg[3],arg[4])
			end,
			["remove"]=function()
				api.remove(arg[2])
			end,
			["rm"]=function()
				api.remove(arg[2])
			end,
			["list"]=function()
				api.getAll()
			end,
			["default"]=function()print("err",arg[1],arg[2])end
	})
	--print(arg[1]..":"..arg[2])
end
main(arg)

api.save()
