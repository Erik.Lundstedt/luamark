#!/bin/env lua
-- -*- tab-width: 4; indent-tabs-mode: t; lua-indent-level: 4; lua-indent-string-contents: t; -*-


local json = require("dkjson")
local bookmark={}
local marks={}
local conf={}
conf["path"]=os.getenv("HOME").."/.config/luamark/bookmarks.json"
local function isempty(s)
	return s == nil or s == ''
end

function bookmark.new(name, uri)
	marks[name] = {}
	marks[name]["name"] = name
	marks[name]["uri"] = uri
end

function bookmark.marks()
	return marks
end

function bookmark.get(name)
	return marks[name]
end

function bookmark.getAll()
   local retval={}
   local i=1
   for k,v in pairs(marks) do
	  retval[i]={["name"]=k,["uri"]=v["uri"]}
	  i=i+1
	end
	return retval
end

function bookmark.list()
	local retval=""
	local i=1
	for k in pairs(marks) do
		if i == 1 then
			retval=k
		else
			retval=retval.."\n"..k
		end
		i=i+1
	end
	return retval
end

function bookmark.remove(name)
	marks[name]=""
	marks[name]=nil
end

function bookmark.load()
local path=conf["path"]
	if not isempty(path) then
		local file=io.open(path,"r")
		io.input(file)
		local markJson=io.read()
		io.close()
		marks = json.decode(markJson)
	end

end
bookmark.load()

function bookmark.save()
	local path=conf["path"]
	if not isempty(path) then
		local markJson=json.encode(marks)
		local file=io.open(path,"w")
		io.output(file)
		io.write(markJson.."\n")
		io.close()
	end
end


local api={}


function api.put(name,path)
	bookmark.new(name,path)
	bookmark.save()
end

function api.get(name)
	bookmark.load()
	return bookmark.get(name)["uri"]
end

function api.getAll()
	bookmark.load()
	return bookmark.getAll()
end

function api.printAll()
	bookmark.load()
	print(api.list())
end

function api.getMarks()
	bookmark.load()
	return bookmark.marks()
end

function api.list()
	bookmark.load()
	return bookmark.list()
end


function api.remove(name)
	bookmark.load()
	bookmark.remove(name)
	bookmark.save()
end


function api.load() bookmark.load() end

function api.save() bookmark.save() end


return api



